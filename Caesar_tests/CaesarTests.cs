﻿using System;
using Caesar;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Caesar_tests
{
    [TestClass]
    public class CaesarTests
    {
        [TestMethod]
        public void Caesar_Test_R() //выход за границы справа
        {
            string expected = "Б";
            string actual = new MainWindow().Caesar(100 ,"А");

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Caesar_Test_L() //выход за границы слева
        {
            string expected = "Я";
            string actual = new MainWindow().Caesar(-100, "А");

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Caesar_Test_Rus() //сдвиг только букв русского алфавита
        {
            string expected = "бВVw l!@>";
            string actual = new MainWindow().Caesar(1, "аБVw l!@>");

            Assert.AreEqual(expected, actual);
        }
    }
}
