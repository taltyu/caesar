﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using Microsoft.Win32;
using Word = Microsoft.Office.Interop.Word;

namespace Caesar
{
    public partial class MainWindow : Window
    {
        public int step=0;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Open_File(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "txt files (*.txt)|*.txt|Word document (*.doc?) |*.doc?";
            dialog.RestoreDirectory = true;
            if (dialog.ShowDialog() == true)
            {
                string fileName = dialog.FileName;
                string fileFormat = fileName.Substring(fileName.LastIndexOf("."));
                Input.Clear();
                if (fileFormat.Equals(".docx") || fileFormat.Equals(".doc"))
                {
                    try
                    {
                        Word.Application wordApp = new Word.Application();
                        wordApp.Visible = false;
                        Word.Document doc = wordApp.Documents.Open(fileName);

                        string text = "";
                        for (int i = 1; i < doc.Paragraphs.Count + 1; i++)
                        {
                            string currString = doc.Paragraphs[i].Range.Text.ToString();
                            text += currString;
                        }
                        doc.Close();
                        wordApp.Quit();
                        Input.Text = text;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else if (fileFormat.Equals(".txt"))
                {
                    try
                    {
                        Input.Text = File.ReadAllText(fileName, Encoding.GetEncoding(1251));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else MessageBox.Show("Неверный формат входных данных");
            }
        }

        private void Save_File(object sender, RoutedEventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "txt files (*.txt)|*.txt|Word document (*.docx) |*.docx";
            save.FileName = "Result";
            if (save.ShowDialog() == true)
            {
                string fileName = save.FileName;
                string fileFormat = fileName.Substring(fileName.LastIndexOf("."));
                if (fileFormat.Equals(".docx") || fileFormat.Equals(".doc"))
                {
                    try
                    {
                        Word.Application wordApp = new Word.Application();
                        wordApp.ShowAnimation = false;
                        wordApp.Visible = false;
                        Word.Document document = wordApp.Documents.Add();
                        document.Content.SetRange(0, 0);
                        document.Content.Text = Output.Text;
                        document.SaveAs2(fileName);
                        document.Close();
                        document = null;
                        wordApp.Quit();
                        MessageBox.Show("Сохранено!");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message.ToString());
                    }
                }
                else if (fileFormat.Equals(".txt"))
                {
                    try
                    {
                        using (StreamWriter sw = new StreamWriter(fileName, false, Encoding.GetEncoding(1251)))
                        {
                            sw.Write(Output.Text, Encoding.GetEncoding(1251));
                            sw.Close();
                            MessageBox.Show("Сохранено!");
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else MessageBox.Show("Неверный формат входных данных");
            }
        }

        private void Left_Click(object sender, RoutedEventArgs e)
        {
            Step.Text = (step - 1).ToString();
            Caesar(step, Input.Text);
        }

        private void Right_Click(object sender, RoutedEventArgs e)
        {
            Step.Text = (step + 1).ToString();
            Caesar(step, Input.Text);
        }

        public string Caesar(int step, string input)
        {
            string output = "";

            while ((step >= 33) || (step <= -33))
            {
                if (step >= 33)
                    step = step - 33;
                else if (step <= -33)
                    step = step + 33;
            }

            string alfphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            string alfphabetU = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
            int m = alfphabet.Length;

            for (int i = 0; i < input.Length; i++)
            {
                if (((int)(input[i]) < 1040) || ((int)(input[i]) > 1103) && ((int)(input[i]) != 1105) && ((int)(input[i]) != 1025))
                    output += input[i];
                else
                {
                    for (int j = 0; j < alfphabet.Length; j++)
                    {
                        if (step >= 0)
                        {
                            if (input[i] == alfphabet[j])
                            {
                                int temp = j + step;
                                while (temp >= m)
                                    temp -= m;
                                output += alfphabet[temp];

                            }
                            else if (input[i] == alfphabetU[j])
                            {
                                int temp = j + step;
                                while (temp >= m)
                                    temp -= m;
                                output += alfphabetU[temp];
                            }
                        }
                        else
                        {
                            if (input[i] == alfphabet[j])
                            {
                                int temp = j + step;
                                if (j + step < 0)
                                    temp = j + step + 33;
                                while (-temp >= m)
                                    temp += m;
                                output += alfphabet[temp];

                            }
                            else if (input[i] == alfphabetU[j])
                            {
                                int temp = j + step;
                                if (j + step < 0)
                                    temp = j + step + 33;
                                while (-temp >= m)
                                    temp += m;
                                output += alfphabetU[temp];
                            }
                        }
                    }
                }
            }
            Output.Text = output;
            return output;
        }

        private void Step_TextChanged(object sender, TextChangedEventArgs e)
        {
            while (!int.TryParse(Step.Text, out step))
            {
                MessageBox.Show("Величина сдвига должна быть числом");
                Step.Text = "0";

            }
            if ((step >= 33) || (step <= -33))
                step = step % 33;
            Caesar(step, Input.Text);
        }

        private void Input_TextChanged(object sender, TextChangedEventArgs e)
        {
            Caesar(step, Input.Text);
        }
    }
}